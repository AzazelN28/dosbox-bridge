const net = require('net')
const { spawn } = require('child_process')
const path = require('path')

function split(value) {
  const list = []
  let insideQuotes = false
    , quotes = ''
    , from = 0
  for (let i = 0; i < value.length; i++) {
    const character = value.charAt(i)
    if (character === '"' || character === "'") {
      if (insideQuotes && quotes === character) {
        insideQuotes = false
      }
      if (!insideQuotes) {
        insideQuotes = true
        quotes = character
      }
    }
    if (character === ' ' && !insideQuotes) {
      list.push(value.slice(from, i))
      from = i + 1
    }
  }
  list.push(value.slice(from))
  return list
}

//console.log(split('git commit -m "Fix minor issues"'))

const server = net.createServer((client) => {
  console.log('DOSBox client connected')
  client.on('data', (data) => {
    const [command, ...args] = split(data.toString('ascii'))
    if (!command) {
      return
    }
    console.log(command, 'args', args)
    const cp = spawn(
      command.replace(/\r\n/g, ''),
      args.map(
        arg => arg
          .replace(/\r\n/g, '')
          .replace(/\//g, '-')
          .replace(/\\/g, '/')
      )
    )
    cp.on('error', (error) => {
      console.log(error)
    })
    cp.stderr.on('data', (data) => {
      console.log(data.toString('utf-8'))
      if (process.platform !== 'win32')
        client.write(data.toString('ascii').replace(/\n/g, '\r\n'))
      else
        client.write(data.toString('ascii'))
    })
    cp.stdout.on('data', (data) => {
      console.log(data.toString('utf-8'))
      if (process.platform !== 'win32')
        client.write(data.toString('ascii').replace(/\n/g, '\r\n'))
      else
        client.write(data.toString('ascii'))
    })
    cp.on('close', (code) => {
      console.log('close', code)
    })
    cp.on('exit', (code) => {
      console.log('exit', code)
    })
  })
})
server.listen(5000)

const dos = spawn('dosbox', ['-conf', 'dosbox-bridge.conf', '.'])
dos.on('close', (code) => {
  process.exit(code)
})
