# DOSBox Bridge

## Cómo ejecutarlo

```
node dosbox-bridge.js
```

## ¿Para qué vale esto?

Principalmente para poder hacer llamadas al S.O sobre el que corre DOSBOX pero en esencia
valdría para hacer cualquier cosa: automatización de tareas desde el S.O a dosbox, llamadas
a APIs, etc.

Made with :heart: by [AzazelN28](https://github.com/AzazelN28)
